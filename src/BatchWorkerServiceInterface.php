<?php

namespace Drupal\batch_services;

use Drupal\Core\TypedData\TranslatableInterface;

/**
 * A service for the Batch API.
 */
interface BatchWorkerServiceInterface {

  /**
   * Sets the internal context array to the given array.
   */
  public function setContext(array &$context);

  /**
   * Gets the page number based on the current id & number of items per page.
   */
  public function getPageNumber() : int;

  /**
   * Method to handle processing of single batch item.
   */
  public function processBatchItem(): void;

  /**
   * Method to handle successful batch processing.
   */
  public function batchProcessingFinished(array $results, string $duration): void;

  /**
   * Method to handle failed batch processing.
   */
  public function batchProcessingFailed(mixed $failure);

  /**
   * Gets the batch title.
   */
  public function getTitle(): TranslatableInterface;

  /**
   * Gets the batch error message.
   */
  public function getErrorMessage(): TranslatableInterface;

}
