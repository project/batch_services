<?php

namespace Drupal\batch_services;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * A service for the Batch API.
 */
class BatchWorkerService implements BatchWorkerServiceInterface {
  /**
   * The number of items per result page.
   */
  const ITEMS_PER_PAGE = 100;

  /**
   * A Drupal Messenger object.
   */
  protected MessengerInterface $messenger;

  /**
   * A service implementing Drupal Translatable interface.
   */
  private TranslatableInterface $translationManager;

  /**
   * The batch context.
   */
  protected array $context;

  public function __construct(MessengerInterface $messenger, TranslatableInterface $translationManager) {
    $this->messenger = $messenger;
    $this->translationManager = $translationManager;
  }

  public function t(string $string, array $args = [], array $options = []): TranslatableInterface
  {
      return $this->translationManager->t($string, $args, $options);
  }

  public function getTitle(): TranslatableInterface
  {
    return $this->t('Processing items...');
  }

  public function getErrorMessage(): TranslatableInterface
  {
    return $this->t('An error was encountered');
  }

  public function calculate(): void
  {
    $progress = $this->getProgress();
    $total = $this->getTotal();
    if ($progress < $total) {
      $this->setPercentFinished($progress / $total * 100);
    }
    else {
      $this->setFinished();
    }
  }

  public function setContext(array &$context): void
  {
    if (!isset($context['sandbox']['current_id'])) {
      $context['sandbox']['current_id'] = 0;
    }
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
    }
    if (!isset($context['results']['total'])) {
      $context['results']['total'] = 0;
    }
    if (!isset($context['results']['created'])) {
      $context['results']['created'] = 0;
    }
    if (!isset($context['results']['updated'])) {
      $context['results']['updated'] = 0;
    }
    if (!isset($context['results']['skipped'])) {
      $context['results']['skipped'] = 0;
    }
    $this->context = &$context;
  }

  /**
   * Sets the batch progress message.
   */
  public function setProgressMessage($message = 'Processing item @current of @total...'): void
  {
    $total = $this->getTotal();
    if ($total) {
      $this->context['message'] = $this->t($message, [
        '@current' => $this->getProgress(),
        '@total' => $total,
      ]);
    }
  }

  /**
   * Marks the batch process as being completely finished.
   */
  public function setFinished(): void
  {
    $this->setProgress($this->getTotal());
    $this->setPercentFinished(100);
  }

  /**
   * Mark the percent of the batch process complete.
   */
  public function setPercentFinished($percentage): void
  {
    if ($percentage < 0 || $percentage > 100) {
      throw new \LogicException(sprintf('Invalid percentage: %s', $percentage));
    }
    $this->context['finished'] = $percentage / 100;
  }

  /**
   * Increment various batch context items data.
   */
  public function increment($category, $key): void
  {
    if (!isset($this->context[$category][$key])) {
      $this->context[$category][$key] = 0;
    }
    $this->context[$category][$key]++;
  }

  /**
   * Gets the current ID of the item being processed.
   */
  public function getCurrentId(): string|int
  {
    return $this->context['sandbox']['current_id'];
  }

  /**
   * Increment the current ID.
   */
  public function incrementCurrentId(): void
  {
    $this->context['sandbox']['current_id']++;
  }

  /**
   * Update the progress of the batch process.
   */
  public function setProgress(string|int $progress): void
  {
    $this->context['sandbox']['progress'] = (int) $progress;
  }

  /**
   * Increment the progress of the batch process.
   */
  public function incrementProgress(): void
  {
    $this->increment('sandbox', 'progress');
  }

  /**
   * Gets the progress of the batch process.
   */
  public function getProgress(): int
  {
    return $this->context['sandbox']['progress'];
  }

  /**
   * Sets the total number of processed items.
   */
  public function setTotal(int $total): void
  {
    $this->context['results']['total'] = $total;
  }

  /**
   * Gets the total number of processed items.
   */
  public function getTotal(): int
  {
    return $this->context['results']['total'];
  }

  /**
   * Increment the created attribute.
   */
  public function incrementCreated(): void
  {
    $this->increment('results', 'created');
  }

  /**
   * Increment the updated attribute.
   */
  public function incrementUpdated(): void
  {
    $this->increment('results', 'updated');
  }

  /**
   * Increment the skipped.
   */
  public function incrementSkipped(): void
  {
    $this->increment('results', 'skipped');
  }

  /**
   * Gets info on the batch process.
   */
  protected function getBatchInfo(): array {
    return [];
  }

  public function getPageNumber() : int
  {
    return (int) ceil(($this->getCurrentId() + 1) / static::ITEMS_PER_PAGE);
  }

  public function processBatchItem(): void
  {
    $this->setFinished();
  }

  public function batchProcessingFinished(array $results, $duration): void
  {
    if (empty($results['created'])) {
      $this->messenger->addStatus($this->t('No new items created.'));
    }
    else {
      $this->messenger->addStatus($this->t('It took @duration to create @count items.', [
        '@count' => $results['created'],
        '@duration' => $duration,
      ]));
    }
  }

  public function batchProcessingFailed($failure): void
  {
    // TODO: revise to make this error message more useful.
    $this->messenger->addError($this->t('Processing failed.'));
  }

}
