<?php

namespace Drupal\batch_services;

/**
 * A service for the Batch API.
 */
class FormHelperService implements FormHelperInterface {

  public function prependSubmitCallbackService(&$renderArrayElement, string $service): void
  {
    array_unshift($renderArrayElement['#submit'], 'batch_services_form_callback');
    if (!isset($renderArrayElement['#submit_args'])) {
      $renderArrayElement['#submit_args'] = [];
    }
    array_unshift($renderArrayElement['#submit_args'], [
      $service,
    ]);
  }

  public function appendSubmitCallbackService(&$renderArrayElement, string $service): void
  {
    $renderArrayElement["#submit"][] = 'batch_services_form_callback';
    $renderArrayElement["#submit_args"][array_key_last($renderArrayElement['#submit'])] = [
      $service,
    ];
  }

  public function createBatch($worker_service_id): void
  {
    /** @var \Drupal\batch_services\BatchWorkerServiceInterface $service */
    $service = \Drupal::service($worker_service_id);
    batch_set([
      'operations' => [
        [
          'batch_services_callback',
          [
            $worker_service_id,
          ],
        ],
      ],
      'finished' => 'batch_services_finished_callback',
      'title' => $service->getTitle(),
      'error_message' => $service->getErrorMessage(),
    ]);
  }

}
