<?php

namespace Drupal\batch_services;

/**
 * A service for the Batch API.
 */
interface FormHelperInterface {

  /**
   * Creates a batch process.
   */
  public function createBatch(string $worker_service_id): void;

  /**
   * Prepends a submit callback service to the given render array element.
   *
   * This method must be used to alter the render array element, given Form API
   * limitations concerning callback function argument support.
   */
  public function prependSubmitCallbackService(mixed &$renderArrayElement, string $service): void;

  /**
   * Appends a submit callback service to the given render array element.
   *
   * This method must be used to alter the render array element, given Form API
   * limitations concerning callback function argument support.
   */
  public function appendSubmitCallbackService(mixed &$renderArrayElement, string $service): void;

}
