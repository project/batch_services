# Batch Services
This module provides interfaces & services to help modernize the Batch API.

No administrative interface is provided by this module.

## Batch Worker Service
The Batch Worker Service provides default batch processing functionality.
`BatchWorkerService::processBatchItem` simply finishes the batch by default.
Please refer to [Custom Batch Worker Services](#custom-batch-worker-services) for
information about defining custom batch worker services.

Batch Worker Services can be appended to a form element `#submit` array
via the `FormHelperInterface::appendSubmitCallbackService` method.
```php
function MODULE_form_FORMID_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\batch_services\FormHelperService $formHelperService */
  $formHelperService = \Drupal::service('batch_services.form_helper');
  $formHelperService->appendSubmitCallbackService($form["submit"], 'batch_services.batch_worker');
}
```

When the form is submitted, `batch_services_form_callback` will load the
relevant Submit Callback Service & call the `createBatch` method. Drupal's core
Batch API will create a batch that will routinely call
`batch_services_callback`, which loads the specified Batch Worker Service &
processes a Batch queue item.

### Custom Batch Worker Services
If you wish to define a custom Batch Worker Service, create a custom module that:

1. Depends on `batch_services` module via `example_module.info.yml`:

    ```yaml
    name: Example Module
    type: module
    package: Custom
    dependencies:
     - batch_services:batch_services
    ```

2. Provides a service extending `BatchWorkerService` via `example_module.services.yml`:

    ```yaml
    services:
      example_module.batch_worker.foo:
        class: Drupal\example_module\Form\Batch\FooBatchWorkerService
        arguments: ['@messenger']
    ```

3. Alters the desired form to call the custom Batch Worker Service at the appropriate time.

    ```php
    function MODULE_form_FORMID_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
      /** @var \Drupal\batch_services\FormHelperService $formHelperService */
      $formHelperService = \Drupal::service('batch_services.form_helper');
      $formHelperService->appendSubmitCallbackService($form['submit'], 'example_module.batch_worker.foo');
    }
    ```

4. Defines a class which extends `BatchWorkerService` & overrides methods if necessary.

    ```php
    <?php
    namespace Drupal\example_module\Form\Batch;
    
    use Drupal\batch_services\BatchWorkerService;
    
    class FooBatchWorkerService extends BatchWorkerService {
      
      public function processBatchItem() {
        $example = ['foo', 'bar', 'baz'];
        $this->setTotal(count($example));
        foreach ($example as $item) {
          if ('baz' === $item) {
            $this->incrementSkipped();
          }
          else {
            $this->incrementCreated();
          }
          $this->incrementProgress();
          $this->incrementCurrentId();
        }
      }
    
    }
    ```

## Road Map
* implement unit test coverage: confirm batches created with the help of this module work as expected 
